import matplotlib.pyplot as plt

x = [-3,-2,-1,0,1,2,3]
y = [9,4,1,0,1,4,9]

plt.title("Test Graph")
plt.axis([-5, 5, -10, 10])
plt.xlabel("X")
plt.ylabel("Y")
plt.plot(x,y)
plt.gca().invert_yaxis()
plt.show()

