# README #

This repository will host all the class notes and homeworks.

### To Update ###

Run `git pull` and it should automatically check for new content.

Remember to do your work in your own personal repo, not in this repo!
