# Scoping

# applies for immutable types, they make a copy for the function
def add_one(s):
    s += 1
    n = 5

n = 1
add_one(n)
print(n)

def add_one(arr):
    for i in range(len(arr)):
        arr[i] += 1
    
##arr = [1,2,3,4,5]
##add_one(arr)
##print(arr)

def count(arr):
    total = 0
    while len(arr) > 0:
        arr.pop()
        total += 1
    return total

arr = [1,2,3,4,5,6,7]
print(count(arr))
print(arr)

