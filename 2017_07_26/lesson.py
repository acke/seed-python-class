# More on lists!
# Generate the first n squares (0,1,4,(n-1)**2)
def squares(n):
    """
    Return a list of the first n squares
    """
    #l = []
    #for i in range(n):
    #    l.append(i*i)
    #return l
    # List comprehension
    return [i*i for i in list(range(n))]

def first_letter(str_arr):
    return [s[0] for s in str_arr]

def add_five(n_arr):
    return [n+5 for n in n_arr]

def convert_to_int(str_arr):
    return [int(i) for i in str_arr]

# Useful input trick
# Imagine we read in line: "20 50"
# We want to assign them to variables x and y
#input = "20 50"
#l = input.split()
#x = int(l[0])
#y = int(l[1])
#x, y = [int(i) for i in input.split()]

# String Formatting
# Print out
# Case #1: 1
# Case #2: 2
# ...
# so on until 100
#for i in range(1,100):
    #print("Case #" + str(i) + ": " + str(i))
#    print("Case #{}: {}".format(i,i))

def sum_normal(n):
    """
    Find the sum of the first n positive integers (starting from 1)
    """
##    total = 0
##    for x in range(1,n+1):
##        total += x
##    return total
##    return sum(range(n+1))
    return int(n*(n+1)/2)

def sum_odd(n):
    """ Find sum of first n odd positive integers """
##    ans = 0
##    for i in range(1,2*n+1,2):
##        ans += i
##    return ans
##    return sum(range(1,2*n+1,2))
    return n*n

def sum_even(n):
    """ Find sum of first n even positive integers """
##    ans = 0
##    for i in range(2,2*n+2,2):
##        ans += i
##    return ans
##    return sum(range(2,2*n+2,2))
##    return 2*sum_normal
    return n*(n+1)

def sum_squares(n):
    """
    Find the sum of the first n squares (starting from 1)
    """
##    arr = [i*i for i in range(1,n+1)]
##    return sum(arr)
    return int(n*(n+1)*(2*n+1)/6)

def sum_exp(n, e):
    """
    Find the sum of the first n numbers to e power (starting from 1)
    """
    arr = [i**e for i in range(n+1)]
    return sum(arr)

