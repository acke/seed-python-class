import requests
from bs4 import BeautifulSoup

starting_point = 1
num_books = 50
books_per_page = 25

main_link = "https://www.gutenberg.org"
search_link = "/ebooks/search/?sort_order=downloads"
html_link_format = "/files/{0}/{0}-h/{0}-h.htm"

booklinks = []

current_book = starting_point
while current_book < num_books+starting_point:
    r = requests.get(main_link+search_link+"&start_index={}".format(current_book),headers=headers)
    s = BeautifulSoup(r.content,"lxml")
    booklinks.extend(s.find_all("li",{"class": "booklink"}))
    print("Parsed books from {} to {}".format(current_book,current_book+books_per_page))
    current_book += books_per_page

current_book = starting_point
for booklink in booklinks[:num_books]:
    link = booklink.a["href"]
    book_num = link[8:]
    html_link = main_link+html_link_format.format(book_num)
    html_link_alt = main_link+html_link_format.format(book_num)+'l'
    html_file = open("books/book{}.html".format(current_book),"wb")
    r = requests.get(html_link,headers=headers)
    if not r.ok:
        r = requests.get(html_link_alt)
    html_file.write(r.content)
    print("Saved book {}".format(current_book))
    html_file.close()
    current_book += 1

