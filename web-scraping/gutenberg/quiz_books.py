import os
import bs4
from bs4 import BeautifulSoup
import random
import pickle

def is_newline(bs_obj):
    if type(bs_obj) == bs4.element.NavigableString and bs_obj.string.isspace():
        return True
    return False

def first_parse_stories():
    path = 'books/'

    num_books = 0
    first_paragraph = {}

    for book_path in os.listdir(path):
        num_books += 1
        print("{} books checked".format(num_books))
        book_handle = open(path+book_path,"r")
        try:
            book_soup = BeautifulSoup(book_handle,"lxml")
        except:
            continue
        print("Starting {}".format(book_path))
        titles = book_soup.find_all('h2')
        ch1 = None
        for title in titles:
            title_text = title.text.lower()
            if "chapter" in title_text:
                ch1 = title.next_sibling
                if ch1 == None:
                    break
                while is_newline(ch1):
                    ch1 = ch1.next_sibling
                break
        if ch1 == None:
            continue
        ch1_content = []
        ch1_text = ch1.text
        while ch1.name != 'h2':
            ch1_content.append(ch1_text)
            ch1 = ch1.next_sibling
            while is_newline(ch1):
                ch1 = ch1.next_sibling
            if ch1 == None:
                break
            ch1_text = ch1.text
        title = book_soup.title.string.strip()
        first_paragraph[title] = ch1_content
        print("Finished {}".format(book_path))


    with open('books.pickle', 'wb') as handle:
        pickle.dump(first_paragraph, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('books.pickle', 'rb') as handle:
    first_paragraph = pickle.load(handle)
book_titles = list(first_paragraph)

while True:
    title = random.choice(book_titles)
    story = first_paragraph[title]
    os.system("clear")
    for p in story[:5]:
        print(p)
        input()
    print("Guess the story:")
    input()
    os.system("clear")
    print(title)
    input()

