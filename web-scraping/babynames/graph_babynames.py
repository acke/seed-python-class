import sys
import pickle
import matplotlib.pyplot as plt

if len(sys.argv) < 2:
    print("Expected name")
    sys.exit()

name = sys.argv[1]
with open("babynames.pickle", "rb") as f:
    babynames = pickle.load(f)
x_arr = []
y_arr = []
for year in range(1880,2017):
    x_arr.append(year)
for year in range(1880,2017):
    if name in babynames[year]:
        y_arr.append(babynames[year][name])
    else:
        y_arr.append(1000)
plt.plot(x_arr,y_arr)
plt.axis([1880,2016,1,1000])
plt.xlabel('Years')
plt.ylabel('Rankings')
plt.title('Rankings for {}'.format(name),fontsize=24)
plt.gca().invert_yaxis()
plt.show()
