import re
import pickle

babynames_dict = {}
for year in range(1880,2017):
    year_dict = babynames_dict[year] = {}
    with open("babynames{}.html".format(year),"r") as handle:
        html = handle.read()
        ranks_names = re.findall(r'<td>(\d+)</td>\s<td>(\w+)</td>\s<td>(\w+)</td>',html)
        for rank, male_name, female_name in ranks_names:
            if male_name in year_dict:
                year_dict[male_name] = min(int(rank), year_dict[male_name])
            else:
                year_dict[male_name] = int(rank)
            if female_name in year_dict:
                year_dict[female_name] = min(int(rank), year_dict[female_name])
            else:
                year_dict[female_name] = int(rank)

with open("babynames.pickle","wb") as f:
    pickle.dump(babynames_dict,f)

