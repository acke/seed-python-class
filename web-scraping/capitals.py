import requests
from bs4 import BeautifulSoup

import random
 
req = requests.get('https://en.wikipedia.org/wiki/List_of_national_capitals_in_alphabetical_order')
soup = BeautifulSoup(req.text, "lxml")

capitals = {}

trs = soup.find_all('tr')
for tr in trs[6:]:
    tds = tr.find_all('td')
    capital = tds[0].a['title']
    country = tds[1].a['title']
    capitals[country] = capital

while True:
    countries = list(capitals)
    rand_country = random.choice(countries)
    print("What is the capital of {}?".format(rand_country)) 
    guess_capital = input()
    if guess_capital.strip() == capitals[rand_country]:
        print("Correct! {} is the capital of {}.".format(guess_capital, rand_country))
    else:
        print("Wrong! {} is the capital of {}.".format(capitals[rand_country],rand_country))
    print()

