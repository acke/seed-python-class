from random import randint

def best_position_trial():
    line = []
    for _ in range(366):
        line.append(randint(1,365))
    seen = set()
    for index, p in enumerate(line):
        if p in seen:
            return index + 1
        else:
            seen.add(p)

num_simulations = 100000
winning_locations = {}
for _ in range(num_simulations):
    idx = best_position_trial()
    if idx in winning_locations:
        winning_locations[idx] += 1
    else:
        winning_locations[idx] = 1

best_idx = 1
most_freq = 0
for idx in winning_locations:
    if winning_locations[idx] > most_freq:
        most_freq = winning_locations[idx]
        best_idx = idx
print(best_idx)
