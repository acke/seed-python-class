from random import randint

def optimal_move(num_dragons, misere):
    if misere:
        num_dragons -= 1
    move = num_dragons % 4
    return move

print("How many dragons?")
num_dragons = int(input())
print("Do you want the red or not? yes or no")
misere = input().lower() == "no"

human_turn = randint(0,1)
while num_dragons > 0:
    if human_turn:
        print("G"*(num_dragons-1) + "R")
        print("How many dragons do you want to take?")
        n = int(input())
        num_dragons -= n
        human_turn = False
    else:
        n = optimal_move(num_dragons,misere)
        if n == 0:
            n = randint(1,3)
        print("Computer takes {}".format(n))
        num_dragons -= n
        print("G"*(num_dragons-1) + "R")
        human_turn = True
