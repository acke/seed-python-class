while True:
    L = int(input())
    if L == 0:
        break
    bends = input().split()
    direction = '+x'
    for bend in bends:
        if bend == 'No':
            continue
        elif bend == '+y':
            if direction == '+y':
                direction = '-x'
            elif direction == '-y':
                direction = '+x'
            elif direction == '+x':
                direction = '+y'
            elif direction == '-x':
                direction = '-y'
        elif bend == '-y':
            if direction == '+y':
                direction = '+x'
            elif direction == '-y':
                direction = '-x'
            elif direction == '+x':
                direction = '-y'
            elif direction == '-x':
                direction = '+y'
        elif bend == '+z':
            if direction == '+z':
                direction = '-x'
            elif direction == '-z':
                direction = '+x'
            elif direction == '+x':
                direction = '+z'
            elif direction == '-x':
                direction = '-z'
            pass
        elif bend == '-z':
            if direction == '+z':
                direction = '+x'
            elif direction == '-z':
                direction = '-x'
            elif direction == '+x':
                direction = '-z'
            elif direction == '-x':
                direction = '+z'
    print(direction)

