def median(a,b,c):
    return a + b + c - max(a,b,c) - min(a,b,c)

T = int(input())
for i in range(T):
    a, b, c = [int(n) for n in input().split()]
    print("Case {}: {}".format(i+1, median(a,b,c)))

