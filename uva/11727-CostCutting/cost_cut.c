#include <stdio.h>

int median(int a, int b, int c){
	int min = a;
	if (b < min)
		min = b;
	if (c < min)
		min = c;
	int max = a;
	if (b > max)
		max = b;
	if (c > max)
		max = c;
	return a + b + c - min - max;

}

int main(){
	int T, tc = 1, a, b, c;
	scanf("%d", &T);
	while (T--){
		scanf("%d %d %d", &a, &b, &c);
		printf("Case %d: %d\n", tc++, median(a,b,c));
	}
	return 0;
}
