rfp_num = 1
while True:
    n, p = [int(i) for i in input().split()]
    if n == 0 and p == 0:
        break
    if rfp_num > 1:
        print()
    for _ in range(n):
        input()
    best_name = None
    best_record = 0
    best_price = 0
    for _ in range(p):
        rfp_name = input()
        rfp_record = input().split()
        d = float(rfp_record[0])
        r = int(rfp_record[1])
        if r > best_record or (r == best_record and d < best_price):
            best_name = rfp_name
            best_record = r
            best_price = d
        for _ in range(r):
            input()
    print("RFP #{}".format(rfp_num))
    rfp_num += 1
    print(best_name)

