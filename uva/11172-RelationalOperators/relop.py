t = int(input())
for _ in range(t):
    a, b = [int(num) for num in input().split()]
    if a < b:
        print('<')
    elif a > b:
        print('>')
    else:
        print('=')
    
