max_cost = 2000010
while True:
    try:
        N, B, H, W = [int(i) for i in input().split()]
    except:
        break
    min_cost = max_cost
    for _ in range(H):
        p = int(input())
        avail_beds = [int(i) for i in input().split()]
        total_cost = p * N
        if total_cost > B:
            continue
        if N <= max(avail_beds) and total_cost < min_cost:
            min_cost = total_cost
    if min_cost == max_cost:
        print("stay home")
    else:
        print(min_cost)

