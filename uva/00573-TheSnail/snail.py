while True:
    H, U, D, F = [int(i) for i in input().split()]
    if H == 0:
        break
    day = 1
    height = U
    climb_dist = U
    fatigue_amount = F / 100 * U
    while height <= H:
        height -= D
        if height < 0:
            break
        climb_dist -= fatigue_amount
        climb_dist = max(0, climb_dist)
        day += 1
        height += climb_dist
    if height > H:
        print("success on day", day)
    else:
        print("failure on day", day)

