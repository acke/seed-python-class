# Regular expressions are powerful tools for string parsing

import re

number = "132 3421"

def is_accepted_number(s):
    match = re.search(r'\d{3}[-\s]\d{4}',s)
    if match:
        return True
    else:
        return False

#print(is_accepted_number(number))

address = "15 Wilson Street"

def is_address(addr):
    match = re.search(r'(\d+)\s+(\w+)\s+(Street|St\.|Avenue|Ave\.)',addr)
    num = match.group(1)
    name = match.group(2)
    print(num, name)
    if match:
        return True
    else:
        return False

#print(is_address(address))

