# Topic List

1. Artificial Intelligence
  - Game playing bots
  - Machine Learning
  - Chatbots (Natural language processing)

2. Data Science
  - Data mining, analysis, visualization
  - Big data

3. Security
  - Cryptography
  - Vulnerabilities in programs

4. Web Development
  - Building websites and stuff

5. Networks
  - Internet, how computer connect, WiFi, ...

6. Graphics
  - How things are rendered on your screen
  - Shadows and cool visual effects

7. Programming Languages

8. Game programming
