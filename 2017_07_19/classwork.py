# Classwork exercises
def factorial(n):
    """
    Returns factorial of n
    5! = 5 * 4 * 3 * 2 * 1
    0! = 1! = 1
    """
    total = 1
    for i in range(2,n+1):
        total *= i
    return total

def average(arr):
    return sum(arr) / len(arr)

def myrange(arr):
    return max(arr) - min(arr)

def median(arr):
    arr.sort()
    if len(arr) % 2 == 1:
        return arr[int((len(arr) - 1) / 2)]
    else:
        mid = int(len(arr) / 2)
        return (arr[mid] + arr[mid-1]) / 2

"""
for i in range(5):
    print(factorial(i))

arr = [3,7,2,1,4,6,5]
# sorted = [1,2,3,4,5,6,7]
print(arr)
print(average(arr))
print(myrange(arr))
print(median(arr))

arr = [2,4,6,8]
print(arr)
print(average(arr))
print(myrange(arr))
print(median(arr))
"""

def mycount(s, char):
    """
    Returns the number of occurences of char in s
    """
    n = 0 
    for c in s:
        if c == char:
            n += 1
    return n

def toupper(s):
    """
    Returns the capitalized version of s
    """
    upper_s = ""
    for c in s:
        upper_s += c.capitalize()
    return upper_s

print(mycount("alakazam",'a')) # should equal 4
print(mycount("alakazam",'b')) # should equal 0
print(mycount("alakazam",'m')) # should equal 1

