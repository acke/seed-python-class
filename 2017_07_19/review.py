# Quick overview of Python syntax

# Variables
a = 5
b = "random string"
c = 5.14

# If conditions
if a < 7:
    print("a is less than 7!")
elif b < 10:
    print("We are in else if")
else:
    print("We are in else")

# While conditions
while a < 10:
    print(a)
    a += 1

# For loop
for i in range(10):
    print(i)

# Functions
def foo(a, b):
    return a + b

# Arrays
a = []
a.append(2)
a.append(5)
b = a.pop() # now b has 5 and a only contains 2
a.append(7)
a = [1,6,4,7,3]
a.sort()

# Arrays are MUTABLE!

# Strings
a = "a b c"
a.split() # returns ['a','b','c']

# Strings are IMMUTABLE!

# Type casting
a = 2.5
b = int(a)
b = float(b)
b = str(b)

