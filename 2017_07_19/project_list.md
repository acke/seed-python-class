# Project List

1. Some creative project using SAT words (Hangman, digital flash cards, digital quiz)

2. Wikipedia related project - map all cities to their capitals and create a program to test this knowledge

3. YouTube related project - music guesser

4. Writing Tic-Tac-Toe, Connect Four, Gomoku (Five in a Row)

5. Guessing book - show first paragraph or page of a book and guess where it's come from

6. Crypto related project - cracking passwords, writing ciphers, decoding ciphers

7. Other miscellaneous if you want

