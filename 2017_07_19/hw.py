"""
Solve and submit:
11172 - Relational Operator
11498 - Division of Nlogonia
11727 - Cost Cutting
"""

def wc1(s):
    """
    Word Count
    Given a string, count how many words are in the string
    "This is a sample string" should return 5
    """
    wordcount = 0
    if len(s) > 0:
        wordcount += 1
    if s[len(s)-1] == " ":
        wordcount -= 1
    for i in range(len(s)-1):
        if s[i] != " " and s[i+1] == " ":
            wordcount += 1
    return wordcount


def wc2(s):
    n = 1
    for c in s:
        if c == " ":
            n += 1
    return n

def wc(s):
    if len(s) <= 0:
        return 0
    wordcount = 0
    seen_char = False
    for c in s:
        if c != ' ':
            seen_char = True
        else: # now we see space
            if seen_char:
                wordcount += 1
                seen_char = False
    if seen_char:
        wordcount += 1
    return wordcount

test_cases = [
    "",
    "  ",
    " aefa efaef ",
    " eaf aef eef eaf e fae"
]

for test in test_cases:
    print(wc(test))