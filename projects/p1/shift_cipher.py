# Shift Cipher
# Takes in a string and replaces every letter with a letter shifted by an input k
# No punctuation, and spaces we leave the spaces

from string import ascii_lowercase as letters

def shift_encrypt(m, k):
    """
    m: message
    k: amount to shift by
    """
    ct = ""
    for c in m:
        if c in letters:
            idx = letters.index(c)
            shifted_idx = (x+k) % 26
            ct_c = letters[y]
            ct += ct_c
        else:
            ct += c
    return ct

def shift_decrypt(c, k):
    """
    c: cipher (encrypted message)
    k: amount that it was shifted by
    """
    m = ""
    for ch in c:
        if ch in letters:
            idx = letters.index(ch)
            shifted_idx = (x-k) % 26
            ct_c = letters[y]
            m += ct_c
        else:
            m += ch
    return m
