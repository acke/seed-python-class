# Passwords that can be bruteforced
from string import ascii_letters
alphanumerical_letters = ascii_letters + "0123456789"

def authenticated(attempt):
    password = "12345"
    if attempt == password:
        return True
    return False

# Tier 1
# for i in range(10):
#     success = authenticated(str(i))
#     if success:
#         print("We found the password: ", i)

# Tier 2
# for c in alphanumerical_letters:
#     success = authenticated(c)
#     if success:
#         print("We found the password: ", c)

# Tier 3
def bruteforce_4():
    for c0 in alphanumerical_letters:
        for c1 in alphanumerical_letters:
            for c2 in alphanumerical_letters:
                for c3 in alphanumerical_letters:
                    word = c0 + c1 + c2 + c3
                    if authenticated(word):
                        print("We found the password: ", word)
                        return
# bruteforce_4()

def bruteforce_5():
    for c0 in alphanumerical_letters:
        for c1 in alphanumerical_letters:
            for c2 in alphanumerical_letters:
                for c3 in alphanumerical_letters:
                    for c4 in alphanumerical_letters:
                        word = c0 + c1 + c2 + c3 + c4
                        print(word)
                        if authenticated(word):
                            print("We found the password: ", word)
                            return

bruteforce_5()

def bruteforce_n(n):
    """ n is length of the password """
    if n == 1:
        for c in alphanumerical_letters:
            if authenticated(c):
                return c
    else:
        pass


# Tiers of difficulty
# 1: Assume 1 letter passwords that are just numbers
# 2: Assume 1 letter passwords that are alphanumerical (includes lower and upper)
# 3: Assume 4 letter passwords that are numerical
# 4: Assume 4 letter passwords that are alphanumerical
# 5: No assumptions on length; assume alphanumerical
# Benchmark how long it would take to bruteforce certain length passwords
# At what point is it infeasible?
