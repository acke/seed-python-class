# Monoalphabetic cipher

from string import ascii_lowercase as letters

key = {
    'a': 'd',
    'b': 'a',
    'c': 'x',
    'd': 'e',
    'e': 'b',
    'f': 'f',
    'g': 'h',
    'h': 'g',
    'i': 'i',
    'j': 'k',
    'k': 'j',
    'l': 'm',
    'm': 'l',
    'n': 'o',
    'o': 'n',
    'p': 'q',
    'q': 'p',
    'r': 'r',
    's': 't',
    't': 's',
    'u': 'v',
    'v': 'w',
    'w': 'u',
    'x': 'c',
    'y': 'z',
    'z': 'y'
}

message = """Alice was beginning to get very tired of sitting by her sister on the
bank, and of having nothing to do. Once or twice she had peeped into the
book her sister was reading, but it had no pictures or conversations in
it, "and what is the use of a book," thought Alice, "without pictures or
conversations?"

So she was considering in her own mind (as well as she could, for the
day made her feel very sleepy and stupid), whether the pleasure of
making a daisy-chain would be worth the trouble of getting up and
picking the daisies, when suddenly a White Rabbit with pink eyes ran
close by her.

There was nothing so very remarkable in that, nor did Alice think it so
very much out of the way to hear the Rabbit say to itself, "Oh dear! Oh
dear! I shall be too late!" But when the Rabbit actually took a watch
out of its waistcoat-pocket and looked at it and then hurried on, Alice
started to her feet, for it flashed across her mind that she had never
before seen a rabbit with either a waistcoat-pocket, or a watch to take
out of it, and, burning with curiosity, she ran across the field after
it and was just in time to see it pop down a large rabbit-hole, under
the hedge. In another moment, down went Alice after it!"""

def monoalph_encrypt(message, key):
    ct = ""
    for c in message.lower():
        if c in letters:
            ct += key[c]
        else:
            ct += c
    return ct

def monoalph_decrypt(cipher, key):
    decrypt_key = {}
    for k, v in key.items():
        decrypt_key[v] = k
    m = ""
    for c in cipher.lower():
        if c in letters:
            m += decrypt_key[c]
        else:
            m += c
    return m

def frequency(cipher):
    """ Print frequency of each letter """
    """ Try to print it in order of most frequent letter downwards """
    freq = {}
    for c in cipher:
        if c in letters:
            if c not in freq:
                freq[c] = 1
            else:
                freq[c] += 1
    return freq

def print_freq_desc(freq):
    freq_table = []
    for key, val in freq.items():
        freq_table.append((val,key))
    freq_table.sort(reverse=True)
    for val in freq_table:
        print("{}: {}".format(val[1], val[0]))

ct = monoalph_encrypt(message,key)
m = monoalph_decrypt(ct,key)
print(ct)
print(m)

print_freq_desc(frequency(ct))
