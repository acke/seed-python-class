from random import shuffle

def is_dead_trial(reroll, double=False):
    # Returns 0 if we live, 1 if we died
    revolver = [0,0,0,0,0,1]
    shuffle(revolver)
    if double:
        bullet_loc = revolver.index(1)
        revolver[(bullet_loc+1) % 6] = 1
    while revolver[0] == 1:
        revolver = [0,0,0,0,0,1]
        shuffle(revolver)
        if double:
            bullet_loc = revolver.index(1)
            revolver[(bullet_loc+1) % 6] = 1
    if reroll:
        shuffle(revolver)
        return revolver[0]
    else:
        return revolver[1]

num_simulations = 100000
num_single_reroll_live = 0
num_single_stay_live = 0
num_double_reroll_live = 0
num_double_stay_live = 0
for _ in range(num_simulations):
    if not is_dead_trial(reroll=True):
        num_single_reroll_live += 1
    if not is_dead_trial(reroll=False):
        num_single_stay_live += 1
    if not is_dead_trial(reroll=True, double=True):
        num_double_reroll_live += 1
    if not is_dead_trial(reroll=False, double=True):
        num_double_stay_live += 1
print("Single Percentage when rerolling:", num_single_reroll_live / num_simulations)
print("Single Percentage when staying:", num_single_stay_live / num_simulations)
print("Double Percentage when rerolling:", num_double_reroll_live / num_simulations)
print("Double Percentage when staying:", num_double_stay_live / num_simulations)
