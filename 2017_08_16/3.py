n = 1
while True:
    is_soln = True
    for m in range(2,10):
        if n % m != m - 1:
            is_soln = False
            break
    if is_soln:
        break
    else:
        n += 1

print(n)
