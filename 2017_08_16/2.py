lightbulbs = [True] * 101

for p in range(2,101):
    for l in range(p,101,p):
        if lightbulbs[l]:
            lightbulbs[l] = False
        else:
            lightbulbs[l] = True

for idx, bulb in enumerate(lightbulbs):
    if bulb:
        print(idx)
