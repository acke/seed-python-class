def natural_numbers():
    i = 0
    while True:
        i += 1
        yield i


def odd_numbers():
    i = 1
    while True:
        yield i
        i += 2

def fib():
    a = 0
    b = 1
    while True:
        yield a
        a, b = b, a + b

