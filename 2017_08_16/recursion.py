# Recursion is when you call a function within itself

def factorial(n):
    # Base case: returns solution for smallest case(s)
    if n == 0:
        return 1
    # Recursive case: assume you have a magical box that solves functions of smaller arguments
    return n * factorial(n-1)

def exp(b,e):
    if e == 0:
        return 1
    return b * exp(b,e-1)

def hanoi(start, end, n):
    """
    start: either 1, 2, or 3
    end: either 1, 2, or 3
    n: any positive integer
    return the sequence of moves to solve the problem
    """
    pass

