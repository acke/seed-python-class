from copy import copy
import sys
#sys.stdout.write("Hello World")

row = [' '] * 3
grid = []
for _ in range(3):
    grid.append(copy(row))

def print_grid(grid):
    for row_num, row in enumerate(grid):
        sys.stdout.write(' ')
        print(' | '.join(row))
        if row_num != len(row)-1:
            print('-'*11)

def check_winner(grid):
    """Return 'X', 'O', None, 'Draw'"""
    winner = None
    for row in grid:
        won = True
        candidate = row[0]
        for symb in row[1:]:
            if candidate != symb:
                won = False
                break
        if won and winner == None:
            winner = candidate

    for col_idx in range(len(grid[0])):
        if grid[0][col_idx] == grid[1][col_idx] == grid[2][col_idx] and winner == None:
            winner = grid[0][col_idx]

    if grid[0][0] == grid[1][1] == grid[2][2] and winner == None:
        winner = grid[0][0]
    elif grid[0][2] == grid[1][1] == grid[2][0] and winner == None:
        winner = grid[0][2]

    if winner != ' ':
        return winner


turn = 'X'
while True:
    print(turn + "'s move (row num, col num):")
    row, col = [int(i) for i in input().split()]
    grid[row][col] = turn
    print_grid(grid)
    winner = check_winner(grid)
    if winner == 'X':
        print('X wins!')
        break
    elif winner == 'O':
        print('O wins!')
        break
    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'
