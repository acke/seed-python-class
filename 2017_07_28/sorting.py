def swap(idx1, idx2, arr):
    tmp = arr[idx1]
    arr[idx1] = arr[idx2]
    arr[idx2] = tmp

def bubble_sort(arr):
    is_sorted = False
    while not is_sorted:
        is_sorted = True
        for i in range(len(arr)-1):
            if arr[i] > arr[i+1]:
                swap(i,i+1,arr)
                is_sorted = False

def selection_sort(arr):
    for i in range(len(arr)):
        min_elt = min(arr[i:])
        min_idx = arr[i:].index(min_elt)
        swap(i,i+min_idx,arr)

def insertion_sort(arr):
    for i, elt in enumerate(arr):
        j = i - 1
        while j >= 0 and elt < arr[j]:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = elt

arr = [1,2,4,2,3,4,5,7,5,8,5,3,4,5,6,3,2,8,9,5,4,3]
bubble_sort(arr)
print(arr)

arr = [1,2,4,2,3,4,5,7,5,8,5,3,4,5,6,3,2,8,9,5,4,3]
selection_sort(arr)
print(arr)

arr = [1,2,4,2,3,4,5,7,5,8,5,3,4,5,6,3,2,8,9,5,4,3]
insertion_sort(arr)
print(arr)
