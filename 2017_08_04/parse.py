string = "<li>Bananas</li><li>Apples</li><li>Cherries</li>"

def parse_fruits(string):
    """ Returns an array of the list items """
    list_items = []
    start_idx = string.find("<li>")
    end_idx = string.find("</li>")
    while start_idx != -1 and end_idx != -1:
        word = string[start_idx+4:end_idx]
        list_items.append(word)
        string = string[end_idx+4:]
        start_idx = string.find("<li>")
        end_idx = string.find("</li>")
    return list_items


print(parse_fruits(string)) # ["Bananas", "Apples", "Cherries"]
